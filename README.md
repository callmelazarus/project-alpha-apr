# Todo Application

A full stack todo-list application that allows users to create their own account, multiple projects, each with their own set of tasks.

Front end: django templates
Back end: model classes, function based views, authentication

![Django!](https://img.shields.io/badge/Django-092E20?style=for-the-badge&logo=django&logoColor=green)

## How to run this application on your computer

1. Fork and clone repo
2. Create and start Virtual Environment
3. Install requirements.txt
4. In a terminal, within your project directory, run `python manage.py runserver`
5. Open up a browser, and type in "http://localhost:8000/accounts/signup/" to create an account and start using the application!

### Demo
![walkthru gif!](/images/walkthru.gif)

## Links and purposes:

http://localhost:8000/accounts/login/
Login page. On this page, a user can Signup as well.

http://localhost:8000/projects/
Projects are listed in list form, and display # of tasks. New Project creation is possible as well.

http://localhost:8000/projects/<id>/
Project Specific Page, which notes all tasks and task data (name, assignee, start/end date, is_completed). A new task can be created from this page

http://localhost:8000/tasks/mine/
List's user specific tasks. On this page, tasks can be noted as completed.

