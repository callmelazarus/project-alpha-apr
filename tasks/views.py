from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, TaskEditForm
from tasks.models import Task


@login_required
def show_tasks(request):
    tasks_by_user = Task.objects.filter(assignee=request.user)
    context = {"tasks_by_user": tasks_by_user}
    return render(request, "tasks/list.html", context)


@login_required
def update_tasks(request, pk):
    specific_task = Task.objects.get(pk=pk)
    if request.method == "POST":
        form = TaskEditForm(request.POST, instance=specific_task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        form = TaskEditForm(instance=specific_task)
    context = {"form": form}
    return render(request, "tasks/list.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            new_task = form.save()
        return redirect("show_project", pk=new_task.project.pk)
    else:
        form = TaskForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)
