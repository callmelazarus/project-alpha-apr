## User info

Local username: jay
password: admin

## Technical notes

Django's built in ORM (object-relational mapping) is what we are using to interact with the application data from various relational databases (SQLite, PostgreSQL, MySQL)

One benefit of Django templates, is the ability to use template inheritance. Template inheritance allows you to build a base “skeleton” template that contains all the common elements of your site and defines blocks that child templates can override.

Notes in Notion

https://balanced-apple-83d.notion.site/M1-Capstone-Project-5f13ea5ec58841f6bb135bf86a2c49cb

## Writing a readme
An overview of the app
The technologies in the app
Screenshots of it running
Directions of how to clone and run your application
